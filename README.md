# Service Builder

[![Go Reference](https://pkg.go.dev/badge/git.tars.one/svc.svg)](https://pkg.go.dev/git.tars.one/svc)

`svc` is a package which tries to factor out some common useful elements when
writing a back-end API service in Go.

The `svc/server` package can be used to abstract HTTP servers further:

* Application handler functions need not worry about the request
  decoding/response encoding operations:
  * Using the `codec.Codec` specification, a route's input and output types can
    be controlled. By default they are both JSON, but can both be changed, and
    need not be the same (i.e. can take in JSON, and return XML).
  * To define a custom encoding (e.g. CSV), implement the `codec.Encoder` and/or
    `codec.Decoder` interfaces.
* Using the `Route.MwStack` field, middleware can be provided to handle
  functions like authentication, analytics, etc.
* A context object is threaded through to the application handler to provide
  access to consistent structured logging and additional request information.
  This context object contains an application specified `AppCtxT` value which
  the application populates every request.
* Using `server.Error`, applications can return detailed error messages which
  are consistently reported to the client, and logged for debugging.
* Application handlers are treated as unsafe, and application crashes are
  properly handled and appropriately reported to the caller.
