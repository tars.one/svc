package codec

import "io"

type Codec struct {
	Input  Decoder
	Output Encoder
}

type Decoder interface {
	Decode(src io.Reader, dst any) error
}
type Encoder interface {
	ContentType() string
	Encode(src any, dst io.Writer) error
}

func Initialize(c *Codec) {
	if c.Input == nil {
		c.Input = JSON{}
	}
	if c.Output == nil {
		c.Output = JSON{}
	}
}
