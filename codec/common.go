package codec

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
)

// tryEncodeError checks if the source is an error, and if so, writes the
// Error() string to the destination.
func tryEncodeError(src any, dst io.Writer) (bool, error) {
	if err, ok := src.(error); !ok {
		return false, nil
	} else {
		_, err := dst.Write([]byte(err.Error()))
		return true, err
	}
}

type JSON struct{}

func (c JSON) Decode(src io.Reader, dst any) error {
	return json.NewDecoder(src).Decode(dst)
}
func (c JSON) Encode(src any, dst io.Writer) error {
	if ok, err := tryEncodeError(src, dst); ok {
		return err
	}

	return json.NewEncoder(dst).Encode(src)
}
func (c JSON) ContentType() string {
	return "application/json"
}

type XML struct{}

func (c XML) Decode(src io.Reader, dst any) error {
	return xml.NewDecoder(src).Decode(dst)
}
func (c XML) Encode(src any, dst io.Writer) error {
	if ok, err := tryEncodeError(src, dst); ok {
		return err
	}

	return xml.NewEncoder(dst).Encode(src)
}
func (c XML) ContentType() string {
	return "application/xml"
}

type Noop struct{}

func (c Noop) Decode(_ io.Reader, _ any) error {
	return nil
}
func (c Noop) Encode(src any, dst io.Writer) error {
	if ok, err := tryEncodeError(src, dst); ok {
		return err
	}

	return nil
}
func (c Noop) ContentType() string {
	return "application/plaintext"
}

type Binary struct {
	EncodeContentType string
}

func (c Binary) Decode(src io.Reader, dst any) error {
	d, ok := dst.(*[]byte)
	if d == nil || !ok {
		return fmt.Errorf("binary decoder expected non-nil *[]byte")
	}
	_, err := src.Read(*d)
	return err
}
func (c Binary) Encode(src any, dst io.Writer) error {
	if ok, err := tryEncodeError(src, dst); ok {
		return err
	}

	s, ok := src.(*[]byte)
	if s == nil || !ok {
		return fmt.Errorf("binary encoder expected non-nil *[]byte")
	}
	_, err := dst.Write(*s)
	return err
}
func (c Binary) ContentType() string {
	if c.EncodeContentType == "" {
		return "application/octet-stream"
	}
	return c.EncodeContentType
}
