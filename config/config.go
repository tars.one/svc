package config

import (
	"fmt"
	"log"
	"reflect"
)

func exitf(fmt string, args ...any) {
	log.Printf(fmt, args...)
}

type Loader interface {
	Load(dst any) error
}

func Load[T any](dst *T, cb func(), ldrs ...Loader) error {
	var t reflect.Type
	{
		var x T
		t = reflect.TypeOf(x)
	}
	if t.Kind() != reflect.Struct {
		return fmt.Errorf("destination must be a pointer to struct, got %s", t.Name())
	}

	for _, ldr := range ldrs {
		if err := ldr.Load(dst); err != nil {
			return fmt.Errorf("loader error: %s", err)
		}
	}

	// run callback function
	if cb != nil {
		cb()
	}

	return nil
}

func MustLoad[T any](dst *T, cb func(), ldrs ...Loader) {
	if err := Load(dst, cb, ldrs...); err != nil {
		panic(err)
	}
}
