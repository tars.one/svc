package config

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
)

type EnvLoader struct{}

func (e EnvLoader) Load(dst any) error {
	fn := "config.EnvLoader.Load"

	t := reflect.TypeOf(dst).Elem()
	v := reflect.ValueOf(dst).Elem()

	numFields := t.NumField()
	for i := 0; i < numFields; i++ {
		field := t.FieldByIndex([]int{i})
		name := t.Name() + "." + field.Name

		envVar, ok := field.Tag.Lookup("env")
		if !ok {
			continue
		}

		if val, ok := os.LookupEnv(envVar); !ok {
			return fmt.Errorf("%s: environment variable '%s' not defined for '%s'",
				fn, envVar, name)
		} else {
			// decode val into the field
			switch f := v.FieldByIndex([]int{i}); f.Kind() {
			case reflect.String:
				f.SetString(val)
			case reflect.Float32, reflect.Float64:
				parsed, err := strconv.ParseFloat(val, 64)
				if err != nil {
					return fmt.Errorf("%s: failed to parse value of '%s' into '%s': %s",
						fn, envVar, name, err)
				}
				f.SetFloat(parsed)
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				parsed, err := strconv.ParseInt(val, 0, 64)
				if err != nil {
					return fmt.Errorf("%s: failed to parse value of '%s' into '%s': %s",
						fn, envVar, name, err)
				}
				f.SetInt(parsed)
			case reflect.Bool:
				parsed, err := strconv.ParseBool(val)
				if err != nil {
					return fmt.Errorf("%s: failed to parse value of '%s' into '%s': %s",
						fn, envVar, name, err)
				}
				f.SetBool(parsed)
			}
		}
	}

	return nil
}
