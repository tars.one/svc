package config_test

import (
	"log"
	"os"
	"testing"

	"git.tars.one/atlas/config"
)

func TestEnvLoaderLoadNoEnvFields(t *testing.T) {
	os.Clearenv()
	ldr := config.EnvLoader{}

	if err := config.Load(&struct{}{}, nil, ldr); err != nil {
		log.Println("[fail] got error (no fields)\n", err)
		t.Fail()
	} else {
		log.Println("[pass] no error (no fields)")
	}

	if err := config.Load(&struct {
		X int `json:"X"`
	}{}, nil, ldr); err != nil {
		log.Println("[fail] got error (0 env set)\n", err)
		t.Fail()
	} else {
		log.Println("[pass] no error (0 env set)")
	}
}

func TestEnvLoaderLoadInt(t *testing.T) {
	os.Clearenv()
	ldr := config.EnvLoader{}

	type X struct {
		Int int `env:"INT"`
	}
	x := &X{}

	if err := config.Load(x, nil, ldr); err == nil {
		log.Println("[fail] no error (0 env set)")
		t.Fail()
	} else {
		log.Println("[pass] got error (0 env set)\n", err)
	}

	for env, exp := range map[string]struct {
		Int int
		Err bool
	}{
		"abc":   {0, true},
		"123":   {123, false},
		"0b111": {0b111, false},
		"0xcc":  {0xcc, false},
	} {
		os.Setenv("INT", env)
		if err := config.Load(x, nil, ldr); err != nil && !exp.Err {
			log.Printf("[fail] got error (INT set to: %s)\n%s\n", env, err)
			t.Fail()
		} else if err := config.Load(x, nil, ldr); err == nil && exp.Err {
			log.Printf("[fail] no error (INT set to: %s)\n%s\n", env, err)
			t.Fail()
		} else if !exp.Err && x.Int != exp.Int {
			log.Printf("[fail] wrong Int value loaded (INT set to: %s): %d\n", env, x.Int)
			t.Fail()
		} else {
			log.Printf("[pass] correct Int value loaded (INT set to: %s): %d", env, x.Int)
		}
	}
}

func TestEnvLoaderLoadStr(t *testing.T) {
	os.Clearenv()
	ldr := config.EnvLoader{}

	type Y struct {
		Str string `env:"STR"`
	}
	y := &Y{}

	os.Setenv("STR", "abcdef")
	if err := config.Load(y, nil, ldr); err != nil {
		log.Println("[fail] got error (STR set to: abcdef)\n", err)
		t.Fail()
	} else if y.Str != "abcdef" {
		log.Println("[fail] wrong Str value loaded (STR set to: abcdef): ", y.Str)
	} else {
		log.Println("[pass] correct Str value loaded (STR set to: abcdef): ", y.Str)
	}
}

func TestEnvLoaderLoadBool(t *testing.T) {
	os.Clearenv()
	ldr := config.EnvLoader{}

	type Z struct {
		Bool bool `env:"BOOL"`
	}
	z := &Z{}

	for env, exp := range map[string]bool{
		"T": true, "t": true, "1": true,
		"F": false, "f": false, "0": false,
	} {
		os.Setenv("BOOL", env)
		if err := config.Load(z, nil, ldr); err != nil {
			log.Printf("[fail] got error (BOOL set to: %s)\n%s\n", env, err)
			t.Fail()
		} else if z.Bool != exp {
			log.Printf("[fail] wrong Bool value loaded (BOOL set to: %s): %t", env, z.Bool)
		} else {
			log.Printf("[pass] correct Bool value loaded (BOOL set to: %s): %t", env, z.Bool)
		}
	}
}
