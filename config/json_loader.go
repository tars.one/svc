package config

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
)

type JSONLoader struct {
	Source io.ReadCloser
	config map[string][]byte
}

func NewJSONLoaderFromFile(path string) (*JSONLoader, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("NewJSONLoaderFromFile: error when opening %s: %s", path, err)
	}

	ldr := &JSONLoader{Source: f}
	if err := ldr.loadConfig(); err != nil {
		return nil, fmt.Errorf("NewJSONLoaderFromFile: error loading config: %s", err)
	}

	return ldr, nil
}

func (l *JSONLoader) loadConfig() error {
	rawConfig := map[string]interface{}{}
	defer l.Source.Close()
	if err := json.NewDecoder(l.Source).Decode(&rawConfig); err != nil {
		return err
	}

	config := make(map[string][]byte)
	for k, v := range rawConfig {
		config[k], _ = json.Marshal(v)
	}

	l.config = config
	return nil
}

func (l *JSONLoader) Load(dst any) error {
	fn := "config.JSONLoader.Load"

	t := reflect.TypeOf(dst).Elem()
	v := reflect.ValueOf(dst).Elem()
	if t.Kind() != reflect.Struct {
		return fmt.Errorf("JSONLoader.Load: expected dst to be a struct pointer")
	}

	numFields := t.NumField()
	for i := 0; i < numFields; i++ {
		field := t.FieldByIndex([]int{i})
		name := t.Name() + "." + field.Name

		jsonVar, ok := field.Tag.Lookup("json-var")
		if !ok {
			continue
		}

		if val, ok := l.config[jsonVar]; !ok {
			return fmt.Errorf("%s: JSON variable '%s' not defined for '%s'",
				fn, jsonVar, name)
		} else {
			if err := json.Unmarshal(val, v.FieldByIndex([]int{i}).Addr().Interface()); err != nil {
				return fmt.Errorf("%s: failed to set JSON variable '%s' for '%s'",
					fn, jsonVar, name)
			}
		}
	}

	return nil
}
