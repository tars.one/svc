package context

import (
	"context"

	"git.tars.one/atlas/logger"
	"github.com/google/uuid"
)

type Ctx[AppCtxT any] struct {
	context.Context
	RequestID uuid.UUID
	Log       logger.Logger

	// App contains an application specified context.
	App AppCtxT
}

func NewContext[AppCtxT any]() Ctx[AppCtxT] {
	ctx := Ctx[AppCtxT]{
		Context:   context.Background(),
		RequestID: uuid.New(),
		Log:       logger.NewLogrusLogger(),
	}
	ctx.Log = ctx.Log.WithFields(logger.Fields{
		"req_id": ctx.RequestID.String(),
	})
	return ctx
}
