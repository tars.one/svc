# Basic Example

This demonstrates simple usage of the `svc` service builder.
It shows how to create and run a server with an endpoint `/add-one` which reads
in an integer from it's HTTP request body, adds one to it, and writes this
response back to the client as XML.

To run it, simply `cd` into this directory, and run `source .env` to read the
environment variables into the shell and then `go run main.go` to run the
server.
