package main

import (
	"net/http"

	"git.tars.one/atlas/server"
)

func addOne(ctx *server.ServerCtx[AppCtx], req *int) (resp *int, err error) {
	x := *req + 1

	// testing out error responses
	if x >= 100 {
		return nil, server.Error{
			Status: http.StatusBadRequest,
			Code:   "x_greater_than_hundred",
		}
	}
	return &x, err
}
