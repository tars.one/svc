package main

import (
	"net/http"

	"git.tars.one/atlas/codec"
	"git.tars.one/atlas/config"
	"git.tars.one/atlas/server"
)

// the server's config
var scfg server.Config

type AppConfig struct {
	Version    string `json-var:"version"`
	MaxThreads int    `json-var:"max_threads"` // for example
}

var appConfig AppConfig

func init() {
	env := config.EnvLoader{}
	json, err := config.NewJSONLoaderFromFile("config")
	if err != nil {
		panic(err)
	}

	// load the server's config from the environment
	config.MustLoad(&scfg, nil, env)
	config.MustLoad(&appConfig, nil, json)
}

type AppCtx struct {
	UserID string
}

func appCtxInit(ctx server.ServerCtx[AppCtx], r *http.Request) AppCtx {
	actx := AppCtx{UserID: r.Header.Get("X-App-UserID")}
	ctx.Log.Tracef("AppCtxT initialized: %+v\n", actx)
	return actx
}

func main() {
	// initizlize the server with the loaded config
	s, err := server.NewServer(scfg, appCtxInit)
	if err != nil {
		panic(err)
	}

	// add some routes
	server.AddRoute(s, &server.Route[int, int, AppCtx]{
		Pattern: "/add-one",
		Method:  http.MethodGet,
		Handler: addOne,
		MwStack: []server.Middleware[AppCtx]{
			server.RequestLoggerMW[AppCtx]{},
		},
		Codec: codec.Codec{
			Input:  codec.JSON{},
			Output: codec.XML{},
		},
	})

	// run the server
	s.Run()
}
