package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

type LogrusConfig struct {
	// one of: panic, fatal, error, info, debug, trace
	LogLevel string `env:"SVC_LOG_LEVEL"`
	// one of: pretty, json
	LogFormat string `env:"SVC_LOG_FORMAT"`
}

var LogrusCfg LogrusConfig

var logLevels = map[string]logrus.Level{
	"panic": logrus.PanicLevel,
	"fatal": logrus.FatalLevel,
	"error": logrus.ErrorLevel,
	"info":  logrus.InfoLevel,
	"debug": logrus.DebugLevel,
	"trace": logrus.TraceLevel,
}

// ============================================================================

type LogrusLogger struct {
	*logrus.Entry
}

// here, we setup the standard logger that will act as the base for all the
// loggers created by this package
func baseLogger() *logrus.Logger {
	logger := logrus.New()

	if level, ok := logLevels[LogrusCfg.LogLevel]; !ok {
		logger.SetLevel(logrus.DebugLevel)
	} else {
		logger.SetLevel(level)
	}

	switch LogrusCfg.LogFormat {
	case "pretty":
		// pretty logging when stdout is TTY
		logger.SetFormatter(&logrus.TextFormatter{})
	default: // json
		// override the default logger field names for time and message + output
		// logs as JSON for easy parsing
		logger.SetFormatter(&logrus.JSONFormatter{
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyTime: "timestamp",
				logrus.FieldKeyMsg:  "message",
			},
			PrettyPrint: true,
		})
	}

	logger.SetOutput(os.Stdout)
	return logger
}

func NewLogrusLogger() Logger {
	logger := baseLogger()
	return &LogrusLogger{logrus.NewEntry(logger)}
}

func (l LogrusLogger) WithFields(fields Fields) Logger {
	return &LogrusLogger{
		l.Entry.WithFields(fields),
	}
}
