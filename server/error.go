package server

import (
	"encoding/json"
	"net/http"

	"git.tars.one/atlas/codec"
)

type ErrCode = string

const (
	MethodPathInvalidErr ErrCode = "method_path_error"
	EncoderErr           ErrCode = "encoder_error"
	DecoderErr           ErrCode = "decoder_error"
	UnknownErr           ErrCode = "unknown_error"
	ApplicationErr       ErrCode = "application_error"
)

// A more verbose way to return an error.
type Error struct {
	Status  int     `json:"status_code"`
	Code    ErrCode `json:"app_err_code"`
	Msg     string  `json:"message,omitempty"`
	Details string  `json:"details,omitempty"`
}

func (e Error) Error() string {
	if e.Code == "" {
		e.Code = ApplicationErr
	}
	res, _ := json.MarshalIndent(e, "", " ")
	return string(res)
}

func handleError(w http.ResponseWriter, codec codec.Codec, err error) {
	status := http.StatusInternalServerError
	if err, ok := err.(Error); ok {
		status = err.Status
	}

	w.WriteHeader(status)

	if err := codec.Output.Encode(err, w); err != nil {
		w.Write([]byte(err.Error()))
	}
}
