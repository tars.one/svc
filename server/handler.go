package server

import (
	"net/http"

	"git.tars.one/atlas/context"
	"git.tars.one/atlas/logger"
	"github.com/gorilla/mux"
)

// ServerCtx  a context which is threaded through all route handlers by a
// Server, and is intended to contain information that the handlers might need
// about the request they are executing.
type ServerCtx[AppCtxT any] struct {
	context.Ctx[AppCtxT]

	Pattern, Path, Method string
	PathVars              map[string]string

	Response ServerResponse
}

type ServerResponse struct {
	Status      int // http.Status*
	ContentType string
}

// RouteHandler is the signature for routes registered with the server.
type RouteHandler[InT, OutT, AppCtxT any] func(
	ctx *ServerCtx[AppCtxT], req *InT,
) (resp *OutT, err error)

// AppCtxInit is the signature for the function which initializes the
// application provided AppCtxT for every request.
type AppCtxInit[AppCtxT any] func(
	ctx ServerCtx[AppCtxT], r *http.Request,
) AppCtxT

func (s *Server[AppCtxT]) registerHandler(pattern, method string, h http.HandlerFunc) {
	// Register a generic handler for this route, which will run when this route
	// (i.e. pattern and match) are match (see `methodSelector`).
	s.handlers[method+pattern] = h
}

func (s *Server[AppCtxT]) lookupHandler(pattern, method string) http.HandlerFunc {
	return s.handlers[method+pattern]
}

func methodSelector[InT, OutT, AppCtxT any](
	s *Server[AppCtxT], route *Route[InT, OutT, AppCtxT],
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if h := s.lookupHandler(route.Pattern, r.Method); h == nil {
			s.log.WithFields(logger.Fields{
				"route_pattern": route.Pattern,
				"route_method":  route.Method,
				"path":          r.URL.Path,
				"method":        r.Method,
			}).Trace("rejecting request -- invalid method/path combination")

			handleError(w, route.Codec, Error{
				Status: http.StatusBadRequest,
				Code:   MethodPathInvalidErr,
				Msg:    "request path/method combination invalid",
			})
		} else {
			s.log.WithFields(logger.Fields{
				"route_pattern": route.Pattern,
				"route_method":  r.Method,
				"path":          r.URL.Path,
			}).Trace("handler found, executing...")
			h(w, r)
		}
	}
}

func genericHandler[InT, OutT, AppCtxT any](
	s *Server[AppCtxT], route *Route[InT, OutT, AppCtxT],
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// initialize the context
		sctx := ServerCtx[AppCtxT]{
			Ctx:      context.NewContext[AppCtxT](),
			Pattern:  route.Pattern,
			Path:     r.URL.Path,
			PathVars: mux.Vars(r),
			Method:   route.Method,

			Response: ServerResponse{
				Status: http.StatusOK,
			},
		}
		sctx.Log = sctx.Log.WithFields(logger.Fields{
			"pattern": route.Pattern,
			"path":    r.URL.Path,
			"method":  route.Method,
		})
		sctx.App = s.appCtxInit(sctx, r)

		s.log.Trace("running route middleware stack")
		for _, mw := range route.MwStack {
			if err := mw.Exec(&sctx, w, r); err != nil {
				sctx.Log.Error("error in middleware stack: ", err)
				handleError(w, route.Codec, err)
				return
			}
		}

		s.log.Trace("decoding request")
		var req InT
		if !IsVoid[InT]() {
			if err := route.Codec.Input.Decode(r.Body, &req); err != nil {
				e := Error{
					Status:  http.StatusBadRequest,
					Code:    DecoderErr,
					Msg:     "failed to decode request",
					Details: err.Error(),
				}
				sctx.Log.Error(e)
				handleError(w, route.Codec, e)
				return
			}
		}

		s.log.Trace("calling application handler")
		var resp *OutT
		var err error
		if exErr := unsafeExec(func() { resp, err = route.Handler(&sctx, &req) }); exErr != nil {
			sctx.Log.WithFields(logger.Fields{"details": "handler panic"}).
				Error(exErr)
			handleError(w, route.Codec, Error{
				Status: http.StatusInternalServerError,
				Code:   ApplicationErr,
				Msg:    "hanlder panic",
			})
			return
		}
		if err != nil {
			sctx.Log.WithFields(logger.Fields{"details": "handler error"}).
				Error(err)
			handleError(w, route.Codec, err)
			return
		}

		skipEncode := false
		if resp == nil {
			sctx.Log.Trace("skipping response encoding -- application response nil")
			skipEncode = true
		} else if IsVoid[OutT]() {
			sctx.Log.Trace("skipping response encoding -- void return type")
			skipEncode = true
		}

		s.log.WithFields(logger.Fields{"status": sctx.Response.Status}).
			Tracef("set response status")
		w.WriteHeader(sctx.Response.Status)

		if !skipEncode {
			sctx.Log.Trace("encoding response")
			w.Header().Add("ContentType", route.Codec.Output.ContentType())
			if err := route.Codec.Output.Encode(resp, w); err != nil {
				e := Error{
					Status:  http.StatusInternalServerError,
					Code:    EncoderErr,
					Msg:     "failed to encode response",
					Details: err.Error(),
				}
				sctx.Log.Error(e)
				handleError(w, route.Codec, e)
				return
			}
		}
	}
}

func unsafeExec(f func()) (err any) {
	defer func() {
		if r := recover(); r != nil {
			err = r
		}
	}()

	f()
	return
}
