package server

import (
	"net/http"

	"git.tars.one/atlas/logger"
)

type Middleware[AppCtxT any] interface {
	Exec(sctx *ServerCtx[AppCtxT], w http.ResponseWriter, r *http.Request) *Error
}

// ============================================================================

// RequestLoggerMW logs some information about a request (such as sender IP)
// when it is accepted by the server.
type RequestLoggerMW[AppCtxT any] struct{}

func (mw RequestLoggerMW[AppCtxT]) Exec(
	ctx *ServerCtx[AppCtxT],
	w http.ResponseWriter, r *http.Request,
) *Error {
	ctx.Log.WithFields(logger.Fields{
		"remote_addr": r.RemoteAddr,
	}).Info("serving request")
	return nil
}

// ============================================================================

// CORSHeaderMW adds the headers required to bypass browsers' CORS policy.
type CORSHeaderMW[AppCtxT any] struct {
	AllowedOrigins []string
}

func (mw CORSHeaderMW[AppCtxT]) Exec(
	ctx *ServerCtx[AppCtxT],
	w http.ResponseWriter, r *http.Request,
) *Error {
	if len(mw.AllowedOrigins) == 0 {
		return nil
	}

	for _, o := range mw.AllowedOrigins {
		w.Header().Add("Access-Control-Allow-Origin", o)
	}
	ctx.Log.WithFields(logger.Fields{
		"allowed_origins": mw.AllowedOrigins,
	}).Info("injecting headers")

	return nil
}
