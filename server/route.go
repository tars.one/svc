package server

import (
	"git.tars.one/atlas/codec"
	"git.tars.one/atlas/logger"
)

type Void struct{}

func IsVoid[T any]() bool {
	var x T
	_, ok := interface{}(x).(Void)
	return ok
}

type Route[InT, OutT, AppCtxT any] struct {
	// should be one of http.Method{Get,Head,Post,Put,Patch} etc.
	Method  string
	Pattern string
	Handler RouteHandler[InT, OutT, AppCtxT]
	Codec   codec.Codec
	MwStack []Middleware[AppCtxT]
}

func AddRoute[InT, OutT, AppCtxT any](
	s *Server[AppCtxT], route *Route[InT, OutT, AppCtxT],
) {
	log := s.log.WithFields(logger.Fields{
		"pattern": route.Pattern,
		"method":  route.Method,
	})

	codec.Initialize(&route.Codec)

	if route.Handler == nil {
		log.Warnf("route handler is nil")
	}
	s.registerHandler(route.Pattern, route.Method, genericHandler(s, route))

	s.mux.HandleFunc(route.Pattern, methodSelector(s, route))

	log.Info("Route added")
}
