package server

import (
	"fmt"
	"net/http"

	"git.tars.one/atlas/logger"
	"github.com/gorilla/mux"
)

type Server[AppCtxT any] struct {
	config   Config
	mux      *mux.Router
	log      logger.Logger
	handlers map[string]http.HandlerFunc

	appCtxInit AppCtxInit[AppCtxT]
}

type Config struct {
	Host string `env:"SVC_HOST"`
	Port int    `env:"SVC_PORT"`
}

func NewServer[AppCtxT any](
	c Config, aci AppCtxInit[AppCtxT],
) (*Server[AppCtxT], error) {
	mux := mux.NewRouter()

	return &Server[AppCtxT]{
		config:     c,
		mux:        mux,
		log:        logger.NewLogrusLogger(),
		handlers:   map[string]http.HandlerFunc{},
		appCtxInit: aci,
	}, nil
}

func (s *Server[AppCtxT]) Run() {
	s.log.WithFields(logger.Fields{
		"host": s.config.Host,
		"port": s.config.Port,
	}).Info("listening")

	panic(http.ListenAndServe(
		fmt.Sprintf("%s:%d", s.config.Host, s.config.Port),
		s.mux))
}
