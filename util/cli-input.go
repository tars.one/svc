package util

import (
	"bufio"
	"fmt"
	"os"
	"slices"
	"strconv"
	"strings"
)

type InputOption = string

var YesNoOpts = []InputOption{"Yes", "No"}

const (
	OptYes = 0
	OptNo  = 1
)

func GetCLIInputStr(prompt string) string {
	fmt.Fprintf(os.Stdout, "==> %s: ", prompt)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)

	if !scanner.Scan() {
		fmt.Println("scan error ", scanner.Err())
		return ""
	}

	return strings.TrimSpace(scanner.Text())
}

func GetCLIOption(prompt string, options []InputOption) int {
	if len(options) == 0 {
		return -1
	}

	fmt.Fprintf(os.Stdout, "==> %s\nOptions:\n", prompt)
	if len(options) > 20 {
		for i := 0; i < len(options); i += 2 {
			if i+1 < len(options) {
				fmt.Fprintf(os.Stdout, "\t%d) %s\t%d) %s\n", i+1, options[i], i+2, options[i+1])
			} else {
				fmt.Fprintf(os.Stdout, "\t%d) %s\n", i+1, options[i])
			}
		}
	} else {
		for i := range options {
			fmt.Fprintf(os.Stdout, "\t%d) %s\n", i+1, options[i])
		}
	}

	return GetCLIIntRange(1, len(options))
}

func GetCLIOptionMap(prompt string, options map[string]string, quitKey, quit string) string {
	if len(options) == 0 {
		return ""
	}

	keys := []string{}
	for k := range options {
		keys = append(keys, k)
	}
	slices.Sort(keys)

	fmt.Fprintf(os.Stdout, "==> %s\nOptions:\n", prompt)
	if len(options) > 15 {
		for i := 0; i < len(keys); i += 2 {
			if i+1 < len(keys) {
				fmt.Fprintf(os.Stdout, "\t%02d) %-35s\t%02d) %-35s\n",
					i+1, options[keys[i]], i+2, options[keys[i+1]])
			} else if quitKey == "" {
				fmt.Fprintf(os.Stdout, "\t%d) %s\n", i+1, options[keys[i]])
			} else {
				fmt.Fprintf(os.Stdout, "\t%02d) %-35s\t%02d) %-35s\n",
					i+1, options[keys[i]], i+2, quit)
			}
		}
	} else {
		for i := range keys {
			fmt.Fprintf(os.Stdout, "\t%d) %s\n", i+1, options[keys[i]])
		}
	}

	hi := len(options)
	if quitKey != "" {
		hi += 1
	}
	input := GetCLIIntRange(1, hi)
	if input == len(options) {
		return quitKey
	}
	return keys[input]
}

// getIntRange prompts the user to input an integer between LO and HI.
func GetCLIIntRange(lo, hi int) int {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanLines)

	var input int64
	for input <= 0 {
		fmt.Fprintf(os.Stdout, "Enter a number between %d and %d: ", lo, hi)
		if !scanner.Scan() {
			fmt.Println("scan error ", scanner.Err())
			return -1
		}

		var err error
		input, err = strconv.ParseInt(NormalizeNumStr(scanner.Text()), 10, 64)
		if err != nil || input > int64(hi) {
			input = -1 // ask again
		}
	}

	return int(input - 1) // zero based index of option selected
}

func GetCLIYesNo(prompt string) int {
	return GetCLIOption(prompt, YesNoOpts)
}

func NormalizeNumStr(s string) string {
	s = strings.TrimSpace(s)
	s = strings.ReplaceAll(s, ",", "")
	return s
}
