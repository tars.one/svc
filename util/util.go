package util

func IfThenElse[T any](cond bool, ifVal, elseVal T) T {
	if cond {
		return ifVal
	}
	return elseVal
}

func IfThenElseFn[T any](cond bool, ifFn, elseFn func() T) T {
	if cond {
		return ifFn()
	}
	return elseFn()
}
